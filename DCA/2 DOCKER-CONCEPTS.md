# Conceitos

Plataforma opensource escrita em golang, assim como muitas outras, o golang é uma linguagem de programação com alta performance. A maioria das aplicações que mexem com container são escritas em go. Serve para criar e administrar ambientes isolados.

Conseguimos garantir ambiente de produção homolagação e desenvolvimento conseguem ter os mesmo componentes ou versões de aplicação. Dessa forma minimizando impacotes para entrega de software, acabando com aquele problema de, **na minha máquina funciona**.

![vm vs container](./pics/containerxvm.jpg)

- Infra estrutura é o nosso hardware ou simplesmente
- Hypervisor é o nosso virtualizador da nossa infra.

Escalonamento vertical é quando aumentamos o recurso das máquinas: mais cpu, mais memoria, mais disco, etc. Essa forma permacemos com menos máquinas e mais recursos.

Escalonamento horizontal é quando ao invés de aumentarmos os recursos, nos aumentamos os nós, ou seja, quantidade de máquinas. Máquinas menores, porem maior quantidade.

## Arquitetura

O Docker utiliza alguns recursos do Kernel Linux para o gerenciamento dos containers e entregar ao usuário uma maneira mais fácil de interação com containers.\
 Vale lembrar que o container não é um recursos Docker, mas do Linux. O Docker precisa de rodar em cima de um kernel Linux. Por isso também é possível subir um container sem utilizar o Docker diretamente. \
É possivel rodar o Docker no windowws? Diretamente não. Mas a instalação do Docker no Windows cria uma máquina virtual com Linux e cria um client no Windows referenciando a esta máquina.

Alguns recursos que o Docker utiliza do Linux para a separação de containers:

![Architetura](./pics/dockerkernelresources.png)

- **Namespaces**:Serve para separar espaços de container, ou seja, fornece o isolamento para os containers, limitando o acesso aos recursos do sistema e outros namespaces. Isso quer dizer que o usuário root dentro de um container é diferente do usuário root dentro de outro container ou até mesmo da sua máquina hospedeira. Dessa forma cada container tem sua própria árvore de processos, sistema de arquivos, conexões de redes, etc.

  - *PID: Process Identifier*. Permite de que um container tenha sua própria árvore de processos.
  - *MNT: Mount*. Garante que cada container tenha seu próprio sistema de arquivo e o modo que compartilham esses arquivos.
    - Controla os pontos de montagem
  - *IPC: Inter Process Comunication.* Isolam a comunicação entre processo, impedindo que processos em diferentes namespaces usem o mesmo intervalo de memória.
  - *NET: Network.* Permite virtualizar a stack de network, podendo cada container ter interfaces de redes, sua própria tabela de roteamento, conjunto de ip privado, firewall, etc.
  - *UTS: UNIX Time Sharing*. Permite que um único sistema pareça ter diferentes homes de hosts e domínimo para seus diferentes processos.

- **Cgroups**: São os control groups e isolam os recursos físicos de uma máquina. Por causa desse recursos vc pode limitar recursos de cpu, memória, disco, dispositivos.

  - *CPU:* Permitime limitar a reserva de cpu para um container.
  - *CPUSET*. Limitar um limites de threds para um container.
  - *Memória*: Permite limitar a memória entre os containers.
  - *Device* Permite que um container tenha acesso a um dispositivo da máquina, exemplo, bluetooth, pendrive, etc.

  - **Networking**: Recursos de comunicação que o docker utiliza para a comunicaçãod dos containers entre si e com um ambiente externo.

  - *bridge:* É a capacidade que o docker tem que criar uma ponte com o a placa de rede do host.
  - *veth*. Virtual Ethernet é a capacidade de criar placas de redes virtuais.
  - *iptables*: Permite criar regras de isolamento de redes, é o firewall que o docker utiliza.

![Architetura](./pics/dockerarchiteture.png)

**Container docker tem como objetivo executa sua função e morrer**, por isso que os nomes dados aos containers costumam ser aleatório, mas não ter apego.

/var/lib/docker é onde ficam todos os dados do docker

## Versões

O Docker possui basicamente duas versões, a versão da comunidade () e a versão empresarial ().

- Community Edition
  - Gratuito e também tem seu código aberto.
  - Apenas em ambientes de desenvolvimento
- Enterprise Edition
  - Empresarial
  - US$750 por nó
  - **UCP** (Universal Control Plane)
  - **DTR** (Docker Trusted Registry)
  - suporte da Docker Inc.
  - Recomendação mínima:
    - 8GB de RAM para nós Managers
    - 4GB de RAM para nós Workers
    - 2vCPUs para nós Managers
    - 10GB `/var` em nós Managers (Minimo de 6GB Recomendado)
    - 500MB`/var` em nós workers \
  - A recomendação para ambientes de produção do Docker EE é:
    - 16GB de RAM para nós Managers
    - 4vCPUs para nós Managers
    - 25 a 100GB de espaço livre em disco.
