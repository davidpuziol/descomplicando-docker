# Environment

Precisamos para todo o curso 3 máquinas e vamos utilizar o vagrant para provisionar rapidamente estas máquinas no virtualbox

Os requisitos necessários:

- [vagrant](https://www.vagrantup.com/docs/installation)
- [virtualbox](https://www.virtualbox.org/wiki/Linux_Downloads)

Em versões mais recentes do virtual box
Umas da configurações que precisei fazer para liberar o range de ips utilizados no virtual box foi.

```bash
sudo mkdir /etc/vbox
sudo echo "* 10.0.0.0/8 192.168.0.0/16" >> /etc/vbox/networks.conf
sudo echo "* 2001::/64" >> /etc/vbox/networks.conf
```

Se vira para instalar!

O arquivo [provision.sh](./provision.sh) é chamado para ser rodado ao final da instalação de cada máquina. Observe que tem uma parte comentada no arquivo que é a instalação automática do docker. Foi comentado de propósito para que vc faça a instalação em ambientes diferentes. Caso contrário somente descomente e pule a etapada de instalação se não for do seu interesse em aprender.

## Baixando os boxs para as vms

```bash
vagrant box add centos/7
vagrant box add ubuntu/focal64
```

para subir todas as vms

```bash
**vagrant up** 
```

para destruir todas as vms

```bash
**vagrant destroy** 
```

para subir somente uma máquina

```bash
**vagrant up worker1** 
```

para destruir somente uma máquina

```bash
**vagrant destroy worker1** 
```
