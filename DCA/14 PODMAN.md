# Podman

![Docker vs Podman](./pics/podman.png)

O Podman (o POD MANager) é uma ferramenta para gerenciar contêineres e imagens, volumes montados nesses contêineres e pods feitos de grupos de contêineres. Totalmente baseada em libpod, uma biblioteca para gerenciamento do ciclo de vida do contêiner que também está contida neste repositório. A biblioteca libpod fornece APIs para gerenciar contêineres, pods, imagens de contêiner e volumes.

Essa incrível ferramenta é um projeto de código aberto que está disponível na maioria das plataformas Linux e reside no GitHub .

>Por fim, o Podman fornece um front-end de linha de comando compatível com Docker que pode simplesmente criar um alias para o Docker cli, **alias docker = podman** .

## Principais diferenças entre Docker e Podman
![Docker vs Podman2](./pics/podman2.png)

- O Docker possui dois blocos principais: Docker CLI e Docker Daemon.

- O Docker Daemon: é um processo constante em segundo plano que ajuda a gerenciar imagens Docker, contêineres, redes e volumes de armazenamento. E ainda existe uma API Docker Engine REST para interagir com o daemon Docker, acessada via protocolo HTTP.

- Docker CLI é o cliente de linha de comando Docker para interagir com o daemon Docker. Naturalmente o usamos quando executamos qualquer comando Docker.

Devido essa arquitetura o Docker possui alguns problemas:

- Docker é executado em um único processo (e todos os processos filhos pertencem a este processo), o que pode resultar em um único ponto de falha;

- Se o Docker Daemon falhar, todos os processos filhos perderão o controle e entrarão no estado órfão;

- Todas as etapas precisam ser executadas pelo root para as operações do Docker.

### Vantagens do Podman
- Enquanto isso o Podman foi criado sem daemon, ainda assim, é possível desenvolver, gerenciar e executar contêineres OCI em seu sistema Linux. Os contêineres podem ser executados como root ou no modo rootless. Com isso, possui alguns benefícios adicionais como:

- Interage diretamente com o registro de imagens, contêineres e armazenamento de imagens;

- O Docker é construído sobre um runtime container chamado runC e usa Daemon, em vez de usar o daemon, o Podman está usando diretamente o runC;

- Não há necessidade de iniciar ou gerenciar um processo de daemon;

- Há compatibilidade entre imagens do Podman e do Docker.