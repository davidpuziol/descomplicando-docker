# Instalação

<https://docs.docker.com/engine/install/>

Se vc seguir a documentação oficial do docker verá que existe instalação por distro linux, tem o passo a passo para cada uma delas.

Vamos deixar claro uma coisa; existe um script de instalação muito fácil, mas somente deve ser usado em ambiente de teste, desenvolvimento, em produção não é recomendado utilizar esse script. Ele baixa o script e roda direto no shell apontado, nesse caso o bash.

Teste na máquina master por exemplo a instalação com o script de conveniência.

**vagrant ssh master** para entrar na máquina master e execute o comando abaixo

```bash
curl -fsSL https://get.docker.com | sh
sudo curl -fsSL https://get.docker.com | bash
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -aG docker vagrant
```

Confira a saída com o comando

```bash
vagrant@worker1:~$ docker system info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Docker Buildx (Docker Inc., v0.8.2-docker)
  compose: Docker Compose (Docker Inc., v2.6.0)
  scan: Docker Scan (Docker Inc., v0.17.0)

Server:
 Containers: 0
  Running: 0
  Paused: 0
  Stopped: 0
 Images: 0
 Server Version: 20.10.17
 Storage Driver: overlay2
  Backing Filesystem: extfs
  Supports d_type: true
  Native Overlay Diff: true
  userxattr: false
 Logging Driver: json-file
 Cgroup Driver: cgroupfs
 Cgroup Version: 1
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
 Swarm: inactive
 Runtimes: runc io.containerd.runc.v2 io.containerd.runtime.v1.linux
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 10c12954828e7c7c9b6e0ea9b0c02b01407d3ae1
 runc version: v1.1.2-0-ga916309
 init version: de40ad0
 Security Options:
  apparmor
  seccomp
   Profile: default
 Kernel Version: 5.4.0-120-generic
 Operating System: Ubuntu 20.04.4 LTS
 OSType: linux
 Architecture: x86_64
 CPUs: 2
 Total Memory: 976.6MiB
 Name: worker1.docker-dca.example
 ID: 5ACX:CQ4L:GD3H:LRFV:NM2J:FVJQ:RA2C:NA3W:WWZE:NSXI:TTTZ:VJ53
 Docker Root Dir: /var/lib/docker
 Debug Mode: false
 Registry: https://index.docker.io/v1/
 Labels:
 Experimental: false
 Insecure Registries:
  127.0.0.0/8
 Live Restore Enabled: false

WARNING: No swap limit support
vagrant@worker1:~$ 
```

## No ubuntu

<https://docs.docker.com/engine/install/ubuntu/>

Teste na máquina ubuntu a instalação como deve ser em um ambiente de produção

**vagrant ssh worker1** para entrar na máquina worker1 e execute o comando abaixo

```bash
 sudo apt-get update
 sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```

Vamos adicionar a chave do repositório do docker para baixar os pacotes direto de lá e logo depois adicionar o repositório

```bash
 sudo mkdir -p /etc/apt/keyrings
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

 echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Depois disso é necessário atualizar o repositório e instalar o docker, o docker cli e o containerd.

O que é o containerd?
Containerd é um projeto graduado no [cncf](https://www.cncf.io). É um container runtime utilizado pelo próprio docker para criar seus containers. É um dos pacotes de instalação obrigatório para o docker bem como para vários outros.

O que podemos entender aqui que o docker é um gerenciador de container, não orquestrador, e precisa de um runtime.

Seria possível instalar somente o containerd e ter suporte aos containers? Sim. Se instalarmos um kubernetes, não precisamos instalar o docker inteiro, somente o runtime.

```bash
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

Poderíamos instalar direto via apt-get install docker pelo repositório do ubuntu diretamente? Sim, vc pode, mas é certo? NÃO. Então se perguntar na prova, já sabe... não deve ser feito dessa maneira.

[Continue](#Continuação)

## No Red Hat e no Centos

<https://docs.docker.com/engine/install/centos/>

epel-release não é necessário, mas é bom, pois são uns pacotes essenciais do s.o.

```bash
sudo yum install yum-utils epel-release  -y
```

Se for CentOS

```bash
 sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```

Se for Red Hat

```bash
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/rhel/docker-ce.repo
```

```bash
sudo yum install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y
```

```bash
sudo systemctl start docker
```

... observe bem e veja que não é muito diferente, somente o processo de adicionar os repositorios e o comando de instalaÇão

## Continuação

é necessário que o usuário tenha permissão para rodar os comandos do docker sem  utilizar o **sudo**.

```bash
sudo usermod -aG docker $USER
```

```bash
sudo systemclt enable docker
sudo systemctl start docker
```

## Parametrização

<https://docs.docker.com/config/daemon/>
<https://docs.docker.com/engine/reference/commandline/dockerd/>

A parametrição do docker daemon no deve ser feita no arquivo

- linux **/etc/docker/daemon.json**
- windows **C:\ProgramData\docker\config\daemon.json**

Esses arquivos não são criados por padrão, vc precisa criá-los.

Toda parametrização requer que o daemon seja recarregado

```bash
sudo systemctl daemon-reload 
```

O docker daemon persiste todos os seus dados dentro de

- linux : **/var/lib/docker**
- windows : **C:\ProgramData\docker**

abaixo uma configuração monstrando tudo que é possível alterar. É só um exemplo.

```json
{
  "allow-nondistributable-artifacts": [],
  "api-cors-header": "",
  "authorization-plugins": [],
  "bip": "",
  "bridge": "",
  "cgroup-parent": "",
  "cluster-advertise": "",
  "cluster-store": "",
  "cluster-store-opts": {},
  "containerd": "/run/containerd/containerd.sock",
  "containerd-namespace": "docker",
  "containerd-plugin-namespace": "docker-plugins",
  "data-root": "",
  "debug": true,
  "default-address-pools": [
    {
      "base": "172.30.0.0/16",
      "size": 24
    },
    {
      "base": "172.31.0.0/16",
      "size": 24
    }
  ],
  "default-cgroupns-mode": "private",
  "default-gateway": "",
  "default-gateway-v6": "",
  "default-runtime": "runc",
  "default-shm-size": "64M",
  "default-ulimits": {
    "nofile": {
      "Hard": 64000,
      "Name": "nofile",
      "Soft": 64000
    }
  },
  "dns": [],
  "dns-opts": [],
  "dns-search": [],
  "exec-opts": [],
  "exec-root": "",
  "experimental": false,
  "features": {},
  "fixed-cidr": "",
  "fixed-cidr-v6": "",
  "group": "",
  "hosts": [],
  "icc": false,
  "init": false,
  "init-path": "/usr/libexec/docker-init",
  "insecure-registries": [],
  "ip": "0.0.0.0",
  "ip-forward": false,
  "ip-masq": false,
  "iptables": false,
  "ip6tables": false,
  "ipv6": false,
  "labels": [],
  "live-restore": true,
  "log-driver": "json-file",
  "log-level": "",
  "log-opts": {
    "cache-disabled": "false",
    "cache-max-file": "5",
    "cache-max-size": "20m",
    "cache-compress": "true",
    "env": "os,customer",
    "labels": "somelabel",
    "max-file": "5",
    "max-size": "10m"
  },
  "max-concurrent-downloads": 3,
  "max-concurrent-uploads": 5,
  "max-download-attempts": 5,
  "mtu": 0,
  "no-new-privileges": false,
  "node-generic-resources": [
    "NVIDIA-GPU=UUID1",
    "NVIDIA-GPU=UUID2"
  ],
  "oom-score-adjust": -500,
  "pidfile": "",
  "raw-logs": false,
  "registry-mirrors": [],
  "runtimes": {
    "cc-runtime": {
      "path": "/usr/bin/cc-runtime"
    },
    "custom": {
      "path": "/usr/local/bin/my-runc-replacement",
      "runtimeArgs": [
        "--debug"
      ]
    }
  },
  "seccomp-profile": "",
  "selinux-enabled": false,
  "shutdown-timeout": 15,
  "storage-driver": "",
  "storage-opts": [],
  "swarm-default-advertise-addr": "",
  "tls": true,
  "tlscacert": "",
  "tlscert": "",
  "tlskey": "",
  "tlsverify": true,
  "userland-proxy": false,
  "userland-proxy-path": "/usr/libexec/docker-proxy",
  "userns-remap": ""
}
```
