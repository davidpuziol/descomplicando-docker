# Tools

Algumas ferramentas que auxiliarão no desenvolvimento e estudo de docker.

## Play With Docker

<https://labs.play-with-docker.com/>

Esse é um ambiente que o docker proporciona durante 4 horas para que vc estude sem precisar ter um hardware bom. É uma iniciativa muito boa.

Faça uma conta no dockerhub ou github para poder utilizar.

Esse projeto consiste em criar docker in docker conhecido como DinD. Basicamente o docker tem uma imagem como um sistem operacional preparado e com docker instalado que vc pode tratá-lo como se fosse um node.

Para níveis de processamento um pouco maior é possível criar um master com um worker e adicionar mais 3 workers, ou 5 managers.

![playwithdocker](./pics/playwithdocker.png)

![playwithdocker2](./pics/playwithdocker2.png)

Para acessar o ssh direto da sua máquina vc pode copiar o endereço e colar no seu terminal. 

>Tive problema e somente adicionando isso no final consegue resolver

```bash
ssh ip172-18-0-33-cb48aop917ag00bjpb2g@direct.labs.play-with-docker.com -oHostKeyAlgorithms=+ssh-rsa -oPubkeyAcceptedAlgorithms=+ssh-rsa
```

Não utilize este ambiente com dados sensíveis e essa linha de ssh não precisa de senha, qualquer um com acesso entra na máquina.

Uma coisa observado é que vc não consegue utilizar o terminal do playwithdocker nos workers.

Quando vc publica uma morta ele aparece la em cima e caso vc clique no link ele redireciona para o serviço

![playwithdocker3](./pics/playwithdocker3.png)

É um cluster completo

## Swarmpit

Tanto o swarmpit quanto o portainer fazem práticamente a mesma coisa. Porém, o portainer tem mais recursos, mas um visual pior.

<https://swarmpit.io/>

Vamos analisar o deploy dele

Pelo site tem todo o método de instalação, mas se vc executar também a stack abaixo ele simplesmente irá instalar igual.

```yaml
version: '3.3'

services:
  app:
    image: swarmpit/swarmpit:latest
    environment:
      - SWARMPIT_DB=http://db:5984
      - SWARMPIT_INFLUXDB=http://influxdb:8086
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
    ports:
      - 888:8080
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:8080"]
      interval: 60s
      timeout: 10s
      retries: 3
    networks:
      - net
    deploy:
      resources:
        limits:
          cpus: '0.50'
          memory: 1024M
        reservations:
          cpus: '0.25'
          memory: 512M
      placement:
        constraints:
          - node.role == manager

  db:
    image: couchdb:2.3.0
    volumes:
      - db-data:/opt/couchdb/data
    networks:
      - net
    deploy:
      resources:
        limits:
          cpus: '0.30'
          memory: 256M
        reservations:
          cpus: '0.15'
          memory: 128M

  influxdb:
    image: influxdb:1.7
    volumes:
      - influx-data:/var/lib/influxdb
    networks:
      - net
    deploy:
      resources:
        limits:
          cpus: '0.60'
          memory: 512M
        reservations:
          cpus: '0.30'
          memory: 128M

  agent:
    image: swarmpit/agent:latest
    environment:
      - DOCKER_API_VERSION=1.35
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
    networks:
      - net
    deploy:
    # Observe que ele tem um agent em cada node
      mode: global
      labels:
        swarmpit.agent: 'true'
      resources:
        limits:
          cpus: '0.10'
          memory: 64M
        reservations:
          cpus: '0.05'
          memory: 32M

networks:
  net:
    driver: overlay

volumes:
  db-data:
    driver: local
  influx-data:
    driver: local
```

O Comando abaixo é oferecido dentro do próprio site deles. Execute em algum dos manager do playwithdocker e veja a porta 888 abrindo e faça um tour pela ferramenta.

```bash
docker run -it --rm \
  --name swarmpit-installer \
  --volume /var/run/docker.sock:/var/run/docker.sock \
swarmpit/install:1.9
```

## Portainer

<https://www.portainer.io/?hsLang=en>
<https://docs.portainer.io/v/ce-2.9/start/install>

 O portainer assim como o docker tem a community edition que é free ou o business edition.

## Harbor

<https://goharbor.io/>

Harbor é um registry open source graduado na CNCF.

Algumas caracteristicas.

- Faz a análise de segurança e vulnerabilidade das imagens
- Assinatura e validação de conteúdo
- Escalável
- UI Web e API Extensível
- Controle de usuário e permissões
